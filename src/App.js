import React, { Component } from 'react';
// import Select from 'react-select';
import './App.css';

const positions = [
    {id: 0, position: 'Point Guard', abbr: 'PG'},
    {id: 1, position: 'Shooting Guard', abbr: 'SG'},
    {id: 2, position: 'Power Forward', abbr: 'PF'},
    {id: 3, position: 'Small Forward', abbr: 'SF'},
    {id: 4, position: 'Center', abbr: 'C'},
];
const players = [
    {id: 1001, positionId: 0, firstName: 'Steph', lastName: 'Curry'},
    {id: 1002, positionId: 0, firstName: 'Chris', lastName: 'Paul'},
    {id: 1003, positionId: 1, firstName: 'Klay', lastName: 'Thomson'},
    {id: 1004, positionId: 1, firstName: 'Dwayne', lastName: 'Wade'},
    {id: 1005, positionId: 2, firstName: 'Anthony', lastName: 'Davis'},
    {id: 1006, positionId: 2, firstName: 'Draymond', lastName: 'Green'},
    {id: 1007, positionId: 3, firstName: 'LeBron', lastName: 'James'},
    {id: 1008, positionId: 3, firstName: 'Kawhi', lastName: 'Leonard'},
    {id: 1009, positionId: 4, firstName: 'Shaquille', lastName: 'Oneal'},
    {id: 1010, positionId: 4, firstName: 'Yao', lastName: 'Ming'},
]

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            roster: {
                position1: null,
                position2: null,
                position3: null,
                position4: null,
                position5: null,
            }
        }
    }

    selectPlayer = (e) => {
        const selectedPlayerId = e.target.value;
        const fieldName = e.target.name

        const roster = {...this.state.roster, [fieldName]: players.find(player => player.id === parseInt(selectedPlayerId))}
        this.setState({ roster });
    }

    getAvailablePlayers = () => {
        const { roster } = this.state;
        const selectedPositions = Object.keys(roster).map((key, index) => !roster[key] ? null : roster[key].positionId);
        // console.log(selectedPositions);
        
        const availablePlayers = players.filter(player => !selectedPositions.includes(player.positionId));
        // console.log(availablePlayers);

        return availablePlayers.map(player => {
            return (                
                <option key={player.id} value={player.id}>
                    { `${player.firstName} ${player.lastName} - ${positions.find(position => position.id === player.positionId).abbr}` }
                </option>
            )
        });
    }

    renderSelectboxes = () => {
        return positions.map(position => {
            const positionNum = 'position' + position.id+1;
            const { roster } = this.state;
            const playerPosition = positions.find(position => !roster[positionNum] ? null : position.id === roster[positionNum].positionId);

            const currPlayer = !roster[positionNum] ? '' : (
                <option key={ roster[positionNum].id } selected>
                    { `${roster[positionNum].firstName} ${roster[positionNum].lastName} - ${playerPosition.abbr}`}
                </option>
            )

            const helperText = !roster[positionNum] ? 'No player selected' : `starting ${playerPosition.position} selected.`;

            return (
                <div key={ position.id } className="select-holder">
                    <label htmlFor={ positionNum }> Player { position.id+1 }: </label>
                    <select name={ positionNum } id={ positionNum } onChange={ this.selectPlayer }>
                        <option>-- Select One --</option>
                        { currPlayer }
                        { this.getAvailablePlayers() }
                    </select><br />
                    <span>{ helperText }</span>
                </div>
            )
        })
    }

    render() {
        return (
            <div className="App">
                <div className="selects-wrapper">
                    <h1>Select your NBA Starting 5</h1>
                    <p>You can only select one player at each position. As you select a position, the other select boxes will be filtered to only show the remaining available players/positions.</p>
                    { this.renderSelectboxes() }
                </div>
            </div>
        )
    }
}

export default App;